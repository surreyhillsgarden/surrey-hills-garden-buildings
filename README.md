Surrey Hills Garden Buildings create useful extra outdoor space as the perfect addition to your home. We provide a complete service including design, supply and installation. From home offices to games rooms to greenhouses, we will design the perfect garden room for you.

Address: Reigate Road, Buckland, Reigate RH2 9RE, United Kingdom

Phone: +44 1737 240579

Website: https://www.surreyhillsgardenbuildings.co.uk
